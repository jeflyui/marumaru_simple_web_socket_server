# Simple Web Socket Server

Node.js app using [Express 4](http://expressjs.com/) and [Socket.IO](http://socket.io/).
Use the Android application below to run the client.

## Demo
You can test the app [Here](https://whispering-waters-56400.herokuapp.com/)

## Running Locally

Make sure you have [Node.js](http://nodejs.org/)

```sh
$ git clone https://gitlab.com/jeflyui/marumaru_simple_web_socket_server.git
$ cd marumaru_simple_web_socket_server
$ npm install
$ node index.js
```

Your app should now be running on [localhost:5000](http://localhost:5000/).


## Other

For more information, see these articles:

- [Simple Android Socket Client](https://gitlab.com/jeflyui/marumaru_simple_android_socket_client/wikis/home)

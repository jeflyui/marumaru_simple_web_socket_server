var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io').listen(http);
var PORT = (process.env.PORT || 5000);
var form = [{title:"nama"}, {title:"umur"}];

app.set('port', PORT);
app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index',{form:form});
});

//SOCKET
//////////////////////////////////////////////////

io.on('connection',function(socket){
    console.log('on user connected '+socket.id);
    socket.emit("update", form);
    socket.on('add',function(data){
        form.push(data);
        io.emit("update",form);
    })
    socket.on('delete',function(index){
        form.splice(index, 1);
        io.emit("update",form);
    })
    socket.on('disconnect',function(){
        console.log('one user disconnected '+socket.id);
    })
})
///////////////////////////////////////////////////////


http.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
